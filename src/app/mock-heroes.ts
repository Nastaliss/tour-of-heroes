import {Hero} from './hero';

export const HEROES: Hero[] = [
  {id: 11, name: 'Batman'},
  {id: 12, name: 'Batmon'},
  {id: 13, name: 'Batmen'},
  {id: 14, name: 'Batmun'},
  {id: 15, name: 'Batmin'},
  {id: 16, name: 'Batmyn'},
  {id: 17, name: 'Batwoman'},
  {id: 18, name: 'Banan'},
  {id: 19, name: 'Manbat'},
];
